import 'dart:convert';
import 'package:bitcoin_ticker/utilities/constants.dart';
import 'package:http/http.dart' as http;
import '../env/env.dart';

class CoinsApi {
  final String _exchangeRateEndpoint = '/v1/exchangerate';

  Future getExchangeRate(String coin, String currency) async {
    coin = coin.toUpperCase();
    currency = currency.toUpperCase();
    final headers = {'X-CoinAPI-Key': Env.kCoinsApiKey};

    final String urlParam = '/$coin/$currency';

    final String urlComplete =
        Env.kCoinsApiBaseUrl + _exchangeRateEndpoint + urlParam;
    print('URL Complete' + urlComplete);
    Uri url = Uri.parse(urlComplete);

    try {
      final response = await http.get(url, headers: headers);
      if (response.statusCode == kHttpRespSuccess) {
        return jsonDecode(response.body);
      } else {
        print(response.body);
      }
    } catch (e) {
      print('Error getting exchange rate:' + e.toString());
    }
    return '';
  }
}
