import 'package:bitcoin_ticker/utilities/coin_data.dart';
import 'package:flutter/cupertino.dart';

import '../api/coins_api.dart';
import '../components/coin_card.dart';

class CoinsApiService {
  Future generateCards(String currency) async {
    CoinsApi coinsApi = CoinsApi();
    List<Widget> coinCards = [];
    try {
      for (String coin in cryptoList) {
        var response = await coinsApi.getExchangeRate(coin, currency);
        CoinCard coinCard = CoinCard(
          coinName: coin,
          currencyName: currency,
          coinValue: response['rate'].toStringAsFixed(2),
        );
        coinCards.add(coinCard);
      }
    } catch (e) {
      print(e);
    }
    return coinCards;
  }
}
