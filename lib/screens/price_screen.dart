import 'dart:io';
import 'package:bitcoin_ticker/services/coins_api_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../utilities/coin_data.dart';

class PriceScreen extends StatefulWidget {
  @override
  _PriceScreenState createState() => _PriceScreenState();
}

class _PriceScreenState extends State<PriceScreen> {
  String selectedCurrency = 'USD';
  CoinsApiService coinsApiService = CoinsApiService();
  Map exchangeRate = {};
  List<Widget> coinCards = [];

  @override
  void initState() {
    super.initState();
    loadCoins();
  }

  void loadCoins() async {
    //add loader
    coinCards = await coinsApiService.generateCards(currenciesList[0]);
    setState(() {
      coinCards = coinCards;
    });
  }

  DropdownButton<String> androidDropdown() {
    List<DropdownMenuItem<String>> dropdownItems = [];
    for (var item in currenciesList) {
      dropdownItems.add(DropdownMenuItem(
        child: Text(item),
        value: item,
      ));
    }

    return DropdownButton<String>(
        value: selectedCurrency,
        items: dropdownItems,
        onChanged: (value) {
          setState(() async {
            exchangeRate = await coinsApiService.generateCards(value!);
          });
        });
  }

  CupertinoPicker getCupertinoPicker() {
    List<Text> items = [];
    for (var item in currenciesList) {
      items.add(
        Text(item),
      );
    }

    return CupertinoPicker(
        backgroundColor: Colors.lightBlue,
        itemExtent: 32,
        onSelectedItemChanged: (selectedIndex) async {
          coinCards = await coinsApiService.generateCards(
            currenciesList[selectedIndex],
          );
          setState(() {
            coinCards = coinCards;
          });
        },
        children: items);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('🤑 Coin Ticker'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: coinCards,
          ),
          Container(
            height: 150.0,
            alignment: Alignment.center,
            padding: EdgeInsets.only(bottom: 30.0),
            color: Colors.lightBlue,
            child: Platform.isIOS ? getCupertinoPicker() : androidDropdown(),
          ),
        ],
      ),
    );
  }
}
