import 'package:envied/envied.dart';

part 'env.g.dart';

@Envied(path: '.env')
abstract class Env {
  @EnviedField(varName: 'COINS_API_BASE_URL')
  static const String kCoinsApiBaseUrl = _Env.kCoinsApiBaseUrl;

  @EnviedField(varName: 'COINS_API_KEY')
  static const String kCoinsApiKey = _Env.kCoinsApiKey;
}
