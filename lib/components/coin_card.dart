import 'package:flutter/material.dart';

class CoinCard extends StatefulWidget {
  final String coinName;
  final String currencyName;
  String coinValue;

  CoinCard(
      {required this.coinName,
      required this.currencyName,
      required this.coinValue});
  @override
  State<CoinCard> createState() => _CoinCardState();
}

class _CoinCardState extends State<CoinCard> {
  @override
  void initState() {
    super.initState();
    print('Coin Name ${widget.coinName}');
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(18.0, 18.0, 18.0, 0),
      child: Card(
        color: Colors.lightBlueAccent,
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 28.0),
          child: Text(
            '1 ${widget.coinName} = ${widget.coinValue} ${widget.currencyName}',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20.0,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
